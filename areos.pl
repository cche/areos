#!/usr/bin/perl -w
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Math::Random;

###############################################
#                                             #
#  C o m m a n d   l i n e   o p t i o n s    #
#                                             #
###############################################

# Declare default variables for GetOption
my $help            = "";
my $userseq         = "";			# user provided reference sequence name
my $size;					# declare the size of the reference sequence to generate
my $refseqout       = "refseq.fa";		# output file for the generated sequence
my $tefile          = "defaultTE.fasta";	# TE file that will be replaced by user provided
my $nummut          = 5;			# number of mutants that will be generated
my $muttime         = 1000000;			# number of generations
my $mutrate         = 1e-8;			# mutation rate
my $mutdist         = "lin";			# Mutation distribution model of type normal (norm), exponential (exp) or linear (lin), default is "lin"
my $indelrate       = 5;			# indel occurrence rate
my $keep            = 0;			# for debugging purposes...
my $readsize        = 0;			# Size of the reads , default = 100.
my $numread;					# Declare number of reads.
my $outSeqFile      = "read1.fq";		# Declare the name of read 5' output file.
my $out2SeqFile     = "read2.fq";		# Declare the name of read 3' output file.
my $id		    = "ILLUMINA";		# Declare the prefix of the reads. (Default is "ILLUMINA")	
my $badqualitysize  = 33;			# Size (in percentage) of bad quality from the end of the read, default=33%.
my $maxInsert       = 0;			# Declare the amplicon average size, default is 500 or 3000 if "-mp" selected.
my $insertVar	    = 10;			# Declare the variation of the amplicon size in percentage, default = 10.	
my $error_rate      = 0;			# Declare the error rate for the bases with low quality, default = 0%.
my $matepair;					# Declare is you want mate pair reads.
my $teoption	    = "yes";			# Declare if you want add TEs.

GetOptions(
	"help|h"	 => \$help,
	"seq|s=s"	 => \$userseq,
	"refsize|rs=i"	 => \$size,
	"seqout|out|o=s" => \$refseqout,
	"tefile|te=s"	 => \$tefile,
	"mut|m=i"	 => \$nummut,
	"gen|g=i"	 => \$muttime,
	"mutrate|mr=i"   => \$mutrate,
	"mutdist|md=s"   => \$mutdist,
	"indelrate|ir=i" => \$indelrate,
	"k=i"            => \$keep,
	"size|lr=i"	 => \$readsize,
	"num|n=i"        => \$numread,
	"read1|r1=s"     => \$outSeqFile,
	"read2|r2=s"     => \$out2SeqFile,
	"bq=i"           => \$badqualitysize,
	"I=i"            => \$maxInsert,
	"iv"             => \$insertVar,
	"mp"             => \$matepair,
	"id=s"		 => \$id,
	"teoption=s"	 => \$teoption,
	"er=i"           => \$error_rate
) or pod2usage(2);

pod2usage(1) if $help;

my $refseq_no_te;
my $refseq_id;

#################
#               #
#  SUBROUTINES	#
#               #
#################

# this subroutine returns a random sequence with a given size
sub getRandSeq {
	my ($rsize) = @_;
	my $returnseq;
	for (my $i = 1 ; $i <= $rsize ; $i++) {
		my $base = randomBase();
		$returnseq .= $base;
	}
	return $returnseq;
}

sub randomBase {
	my @fourbases = ('A', 'C', 'G', 'T');
	return $fourbases[int(rand(4))];
}

# Inserts a sequence into refseq at a given position
sub insertSeq {
	my ($nseq, $refseq, $pos) = @_;
	my $reflength   = length($refseq);
	my $subseqleft  = substr($refseq, 0, $pos);
	my $subseqright = substr($refseq, $pos, $reflength - $pos);
	my $nrefseq     = $subseqleft . $nseq->seq() . $subseqright;
	return $nrefseq;
}

# Mutate a base
sub mutateBase {
	my ($seq, $pos) = @_;
	my $currbase = substr($seq, $pos, 1);
	my $newbase;
	do {
		$newbase = randomBase();
	} until ($newbase ne $currbase);
	substr($seq, $pos, 1, $newbase);
	return $seq;
}

sub insertIndel {
	my ($seq, $ipos, $size) = @_;
	my $insert = int(rand(2));
	if ($insert) {
		substr($seq, $ipos, 1, getRandSeq($size));
	} else {
		substr($seq, $ipos, $size, "");
	}
	return $seq;
}

sub mutateSeq {
	my ($obj, $perr, $indrate, $time, $num) = @_;

	#calculate the total number of mutations
	my $slen           = $obj->length();
	my $genspermut     = 1.0 / ($perr * $slen);
	my $totalmutations = int($time / $genspermut);

	# mutate the sequence
	my $seq = $obj->seq();
	for (my $i = 0 ; $i < $totalmutations ; $i++) {
		my $pos = int(rand(length($seq)));
		$seq = mutateBase($seq, $pos);

		# insert some indels
		if ($i % $indrate == 0) {
			my $indelsize = int(rand(10) + 1);
			my $ipos      = int(rand(length($seq)));
			$seq = insertIndel($seq, $ipos, $indelsize);
		}
	}

	# Create a new PrimarySeq object to return
	return Bio::PrimarySeq->new(-id => $obj->id . "mut" . $num, -seq => $seq);
}

# This subroutine sets the qualities os the bases, and the error for these bases
sub putquality {
	my ($read, $perc) = @_;
	my $readqual;
	my $newseq;
	my $badbase = $readsize * (100 - $perc) / 100;
	my @seq_read = split("", uc($read));
	for (my $i = 0 ; $i < length($read) ; $i++) {
		my $basequal = 74;
		my $base     = $seq_read[$i];
		if ($i > $badbase) {
			my $al_error = int(rand(100));
			if ($al_error <= $error_rate) {
				my $newbase;
				do {
					$newbase = randomBase();
				} 	while $newbase eq $base;
				$base = $newbase;
			}
			my $qualminus = int(rand($i - $badbase + 1));
			if ($qualminus > 41) {
				$qualminus = 41 ;
			} 
			$basequal -= $qualminus;
		}
		if (not defined $readqual) {
			$readqual = chr($basequal);
			$newseq   = $base;
		} else {
			$readqual .= chr($basequal);
			$newseq .= $base;
		}
	}
	return ($newseq, $readqual);
}

# reverse complement a sequence
sub revcompl {
	my ($dna) = @_;
	my $revcomp = reverse($dna);
	$revcomp =~ tr/ACGTacgt/TGCAtgca/;
	return $revcomp;
}

# Let's start the work!

#####################################
#                                   #
#    M A I N  part of the script    #
#                                   #
#####################################

# check if we have the necessary options
if (($userseq eq "" and not defined $size) or not defined $numread) {
	pod2usage("$0: you should use either -s or -rs AND -n");
}

if (not ($teoption eq "yes" or $teoption eq "no")) {
	pod2usage("$0: ERROR you should use 'yes' or 'no' in the option -teoption default is 'yes'");
}

# parse the mutation distribution parameters
my ($mtype, $av, $sd) = split(",", $mutdist);
if (not ($mtype eq "lin" or $mtype eq "norm" or $mtype eq "exp")) {
	pod2usage("$0: Mutation distribution '-md' should be 'lin', 'exp' or 'norm'.");	
}

# get or produce the reference sequence
if ($userseq ne "") {
	my $refin = Bio::SeqIO->new(-file => $userseq, -format => 'fasta');
	my $refseq_obj = $refin->next_seq();
	$refseq_no_te = $refseq_obj->seq();
	$refseq_id    = $refseq_obj->display_id();

} else {
	$refseq_no_te = getRandSeq($size);
	$refseq_id    = "reference_seq";
}

# get the TE file
if ($teoption eq "yes"){
	my $tes = Bio::SeqIO->new(-file => $tefile, -format => 'fasta'); 
	# generate te mutated sequences
	my @alltes;
	my $teobj;
	while (my $teseq = $tes->next_seq()) {
		my @times;
		if ($mtype eq "norm") {
			if (not defined $av) {
				$av = $muttime/3.0;
			}
			if (not defined $sd) {
				$sd = ($muttime-$av)/2;
			}
			my @pretimes = Math::Random::random_normal($nummut, $av, $sd);
			foreach my $x (sort {$b <=> $a} @pretimes) {
				$x = $x<0 ? 0 : $x;
				push(@times, int($x)); 
			}
		} elsif ($mtype eq "exp") {
			my @pre = Math::Random::random_exponential($nummut, (1/0.3672)/10);
			foreach my $x (sort {$b <=> $a} @pre) {
				my $mm = $muttime*$x;
				my $nmut = $mm > 0 ? int($mm) : 0;
				push(@times, $nmut);
			}
		} else {
			my @unordertimes;
			for (my $n = 0; $n < $nummut; $n++) {
				push(@unordertimes, int(rand($muttime)));
			}
			@times = sort {$b <=> $a} @unordertimes;
		}
		my $i = 0;
		foreach my $t (@times) {
			# mutate the sequence and store the obj in a list
			$i++;
			push(@alltes, mutateSeq($teseq, $mutrate, $indelrate, $t, $i));
		}
	}

	if ($keep == 1) {
		my $tefileout = Bio::SeqIO->new(-file => ">alltes.fa", -format => "Fasta");
		foreach my $te_mut_out (@alltes) {
			$tefileout->write_seq($te_mut_out);
		}
	}

# Insert all mutated sequences into the refseq
	my @gffpos;
	my $i = 0;
	foreach my $te (@alltes) {
		my $inpos = int(rand(length($refseq_no_te)));
		$refseq_no_te = insertSeq($te, $refseq_no_te, $inpos);
		foreach my $gffline (@gffpos) {
			if ($gffline->start > $inpos) {
				$gffline->start($gffline->start + $te->length);
			}
			if ($gffline->end > $inpos) {
				$gffline->end($gffline->end + $te->length);
			}
		}
		push(
			@gffpos,
			Bio::SeqFeature::Generic->new(
				-seq_id  => $refseq_id,
				-source  => "readcreator",
				-primary => "transposable_element",
				-start   => $inpos+1,
				-end     => $inpos + $te->length,
				-strand  => "1",
				-frame   => ".",
				-tag     => { ID => $i++, Name => $te->id() }
			)
		)
	}

# generate a gff file with the insertions localizations
	my $gffout;
	$gffout = new Bio::Tools::GFF(-file => ">insertions.gff", -gff_version => 3);
	foreach my $feat (@gffpos) {
		$gffout->write_feature($feat);
	}
	$gffout->close;

# Save the generated sequence
	my $refseqobj;
	my $fileout;
	$refseqobj = Bio::PrimarySeq->new(-id => "reference_seq", -seq => "$refseq_no_te");
	$fileout = Bio::SeqIO->new(-file => ">$refseqout", -format => 'Fasta');
	$fileout->write_seq($refseqobj);
}

# Define name of reads 
if ($matepair) {
	$outSeqFile  = "mp_" . $outSeqFile;
	$out2SeqFile = "mp_" . $out2SeqFile;
} else {
	$outSeqFile  = "pe_" . $outSeqFile;
	$out2SeqFile = "pe_" . $out2SeqFile;
}

open OUT1, ">$outSeqFile";
open OUT2, ">$out2SeqFile";

# define the defaults for pair-ends and mate-pairs if needed.
if ($matepair) {
	if ($maxInsert==0) {
		$maxInsert = 3000;
	}
	if ($readsize==0) {
		$readsize = 100;
	}
} else {
	if ($maxInsert==0) {
		$maxInsert = 500;
	}
	if ($readsize==0) {
		$readsize = 100;
	}
}

# Loop to generate the reads
my $chrlen;
$chrlen = length($refseq_no_te);
my $insvar = ($insertVar/100.0)*2;
for (my $i = 1 ; $i <= $numread ; $i++) {
	my $aleatorio  = int(rand($chrlen - ($maxInsert * (1 + $insvar))));
	my $aleatorio2 = int(rand($maxInsert*$insvar));
	my $lbeg       = $aleatorio;
	my $rbeg       = $aleatorio + ($maxInsert * (1-($insvar/2))) + $aleatorio2;
	my $leftread;
	my $rightread;
	$leftread  = substr($refseq_no_te, $lbeg, $readsize);
	$rightread = substr($refseq_no_te, $rbeg, $readsize);
	if ($matepair) {
		$leftread = revcompl($leftread);
	} else {
		$rightread = revcompl($rightread);
	}		
	my ($seqleft,  $seqleftqual)  = putquality($leftread,  $badqualitysize);
	my ($seqright, $seqrightqual) = putquality($rightread, $badqualitysize);
	my $id_final;
	if ($matepair){
		$id_final = "$id" . "_MP:1:1:0:" . "$i" . "#0";	
	} else {
		$id_final = "$id" . "_PE:1:1:0:" . "$i" . "#0";	
	}	
	print OUT1 "\@$id_final/1\n";
	print OUT1 uc($seqleft)."\n";
	print OUT1 "+\n";
	print OUT1 "$seqleftqual\n";

	print OUT2 "\@$id_final/2\n";
	print OUT2 uc($seqright)."\n";
	print OUT2 "+\n";
	print OUT2 "$seqrightqual\n";
}

__END__

=pod

=head1 NAME
areos.pl

=head1 SYNOPSIS

areos.pl -s <genome_file> -n <number_of_reads> [options]

areos.pl -rs <size_of_generated_sequence> -n <number_of_reads> [options]

To get the complete list of options use

areos.pl -h

=head1 EXAMPLES

In order to produce 5.000.000 reads from your genome of choice
using the default transposable element file:

	areos.pl -s your_genome_sequence.fasta -teoption -n 5000000

In order to produce 10.000.000 reads from your genome of choice
using using your favorite set of transposable elements:

	areos.pl -s your_genome_sequence.fasta -n 10000000 -teoption -te your_TE_file.fasta

In order to create 50 million reads from a 10 Mb reference sequence generated by the program, using the default transposable element file:

	areos.pl -rs 10000000 -n 50000000

=head1 OPTIONS

=over 12

=item C<-help|-h>

Print help.

=item C<-seq|-s>

Genomic sequence file (in fasta format). Can not be used at the same time as -rs.

=item C<-refsize|-rs>

Size of reference sequence that will be generated without the insertions(in bases). Can not be used at same time as -s.

=item C<-seqout|-o>

Output file for the generated sequence. Please provide the basename with a suitable extention like ".fa" or ".fasta", default is "refseq.fa".

=item C<-teoption>

Set if you want add TEs in Reference sequence, "yes" or "no", default=yes. RECOMMENDED. 

=item C<-tefile|-te>

File containing TEs that will be mutated and inserted into the sequence.

=item C<-mut|-m>

Number of mutants that will be generated, default = 5.

=item C<-gen|-g>

Number of generations to simulate, default = 1000000.

=item C<-mutrate|-mr>

Mutation rate, default = 1e-8.

=item C<-mutdist|-md>

Type of Mutation distribution model, normal (norm), exponencial (exp) and linear (lin), default is "lin".

=item C<-indelrate|-ir>

Indel occurrence rate, default = 5.

=item C<-mp>

By default pair-end sequences are produced. If you want simulation of mate-pair reads, use the -mp flag. It will be of the users responsibility to set an appropiate amplicon size.

=item C<-size|-lr>

Size of the generated reads (default is 100).

=item C<-num|-n>

Number of reads.

=item C<-read1|-r1>

Suffix name of 5' reads output file. Please provide the basename with a suitable extention like ".fq". The prefix "pe_" will be prepended if pair ends are generated and "mp_" will be prepended if mate-pairs are generated (default is pe_read1.fq for paired end and mp_read1.fq if  the"-mp"  flag is used).

=item C<-read2|-r2>

Suffix name of 3' reads output file. Please provide the basename with a suitable extention like ".fq". The prefix "pe_" will be prepended if pair ends are generated and "mp_" will be prepended if mate-pairs are generated (default is pe_read2.fq for paired end and mp_read2.fq if  the"-mp"  flag is used).

=item C<-id>

Id prefix name of the reads, dafault is "ILLUMINA". 

=item C<-bq>

Size (in percentage) of bad quality from the end of the read, default=33.

=item C<-I>

Average amplicon size (in bp), default is 500 or 3000 if "-mp" selected.

=item C<-iv>

Amplicon size variation (in percentage) arround the average size, default is 10% bigger or greater.

=item C<-er>

Error rate for the bases with low quality, default = 0%.

=item C<-k>

For debugging purposes...

=back

=head1 DESCRIPTION

This script simulates paired-end or mate-pair short reads as would be generated by an Illumina platform.
The user can provide a genomic sequence or the programme will create a Random DNA Reference Sequence.

=head1 AUTHOR

Cristian Chaparro	- cris.chaparro@gmail.com
Daniel Farias		- fariasdr@gmail.com

=cut
